#!/bin/bash

options="None"
while test $# -gt 0; do
  case "$1" in
    -h|--help)
      echo -e "util to handle webmarks (executable web links)"
      echo -e "\n-h --help 	    show brief help"
      echo -e "\n-b --browser 	    name of the browser"
      echo -e "\n-o --options      string of options to tell browser"
      echo -e "\n-n --name 	    name of the mark"
      echo -e "\n-a --addr		    url address"
      echo -e "\n-i --identifier	unicue name for files in system"
      exit 1
      ;;
    -o|--options)
      shift
      if test $# -gt 0; then
        export options=$1
      else
        echo "hasnt given options"
	exit 1
      fi
      shift
      ;;
    -b|--browser)
      shift
      if test $# -gt 0; then
        export browser=$1
      else
        echo "hasnt given browser"
	exit 1
      fi
      shift
      ;;
    -n|--name)
      shift
      if test $# -gt 0; then
        export name=$1
      else
        echo "hasnt given name"
	exit 1
      fi
      shift
      ;;
    -i|--identifier)
      shift
      if test $# -gt 0; then
        export identifier=$1
      else
        echo "hasnt given identifier"
	exit 1
      fi
      shift
      ;;
    -a|--address)
      shift
      if test $# -gt 0; then
        export addr=$1
      else
        echo "hasnt given address"
	exit 1
      fi
      shift
      ;;
    *)
      shift
      continue
      ;;
  esac
done

script="bash create_webmark.sh -b $browser -a $addr"

if [ $options != "None" ]; then
   script="$script $options"
fi

mark=$($script)

bash put_webmark.sh -m "$mark" -n "$name" -i "$identifier"
bash put_icon.sh -a "$addr" -i "$identifier"

