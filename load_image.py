import requests
from bs4 import BeautifulSoup

from itertools import product


class NoLogoInTitleException(Exception):
    pass


def get_logo_url(address):
    try:
        page = requests.get(address)
    except requests.exceptions.MissingSchema:
        address = "https://" + address
        page = requests.get(address)

    soup = BeautifulSoup(page.content, 'html.parser')
    links = soup.select("link")

    server = next(filter(lambda x: '.' in x, address.split("/")))
    try:
        location = next(filter(lambda x: 'icon' in x.get("rel")[0], links)).get("href")

        url = "http://" + server + location

    except StopIteration:
        sizes = (".", "-32.", "-64.", "-128.", "-180.", "-192.", "-57.", "-76.", "-96.", "-120.", "-144.", "-152.", "-167.", "-195.", "-196.", "-228.")
        suffixes = ("png", "ico", "jpg")
        for si, su in product(sizes, suffixes):
            end = si + su
            location = f"/favicon{end}"
            url = "http://" + server + location
            print(url)
            if requests.get(url).status_code != 404:
                break
        else:
            raise NoLogoInTitleException

    return url


def load_logo(destination, address):
    try:
        url = get_logo_url(address)
        r = requests.get(url)
        with open(destination, 'wb') as f:
            f.write(r.content)

    except NoLogoInTitleException:
        with open(destination, 'wb') as out, open("./src/default.png", 'rb') as inp:
            out.write(inp.read())


load_logo(*input().split())

